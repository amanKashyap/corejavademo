package coreJavaAssignments;

public class multiThreadingExample 
{
	public static void main(String[] args) throws InterruptedException 
	{
		myRunnable runnable = new myRunnable();
		Thread thread = new Thread(runnable);
		System.out.println("Main Thread-->"+Thread.currentThread().getName());
		thread.start();
		/*
		 * NO New Thread Formed
		 * thread.run();
		 * runnable.run();
		 */
		
		Thread.sleep(1000);
		System.out.println("End of Main");
		
	}

}

class myRunnable implements Runnable
{
	@Override
	public void run() 
	{
		System.out.println("Thread in run()-->"+Thread.currentThread().getName());
		for(int i=0; i<10; i++)
		{
			System.out.println("Good Morning");
		}		
	}
	
}