package coreJavaAssignments;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DuplicatesInArray 
{
	public static void main(String[] args) 
	{
		int[] intArray = {1,1,2,3,4,5,6,7,8,8};
		
		Set<Integer> uniqueSet = new HashSet<>();
		List<Integer> duplicateList = new ArrayList<>();
		for(int i : intArray)
		{
			if(!uniqueSet.add(i))
			{
				duplicateList.add(i);
			}
		}
		
		System.out.println("Duplicates are: "+ duplicateList);

	}

}
