package coreJavaAssignments.concurrencyAssignment;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ArrayBlockingQueueExample 
{
	public static void main(String[] args) throws InterruptedException 
	{
	 BlockingQueue<Integer> arrayBlockingQ = new ArrayBlockingQueue<>(100);
	 /*
	  * Duplicates Allowed
	  * Insertion Order
	  * If Try to add elements more than capacity operation fails no exception
	  */
	 arrayBlockingQ.put(1);
	 System.out.println("After Adding 1:"+arrayBlockingQ);
	 for(int i=1; i <10; i++)
	 {
		arrayBlockingQ.put(i);
	 }
	 System.out.println("After Adding 0-100"+arrayBlockingQ);
	 
	 //Retrives and removes Head of Queue
	 System.out.println(arrayBlockingQ.take());
	 
	 arrayBlockingQ.offer(1);
	 System.out.println("Offer 100:-" + arrayBlockingQ);
	 
	 System.out.println("Remaining Capacity:"+arrayBlockingQ.remainingCapacity());
	 
	}

}
