package coreJavaAssignments.concurrencyAssignment;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class LinkedBlockingQueueExample {

	public static void main(String[] args) throws InterruptedException 
	{
		BlockingQueue<Integer> linkedBlockingQueue = new LinkedBlockingQueue<>(10);
		
		for(int i= 0; i <10; i++)
		{
			linkedBlockingQueue.put(i);
		}
		System.out.println("After Adding 10 Elements:");
		System.out.println(linkedBlockingQueue);

		System.out.println("Trying to Add 11th Element");
		System.out.println("Successful: "+linkedBlockingQueue.offer(10));
	}

}
