/**
 * 
 */
package coreJavaAssignments;

/**
 * @author Aman Kashyap
 *
 */
public enum ThreadStates 
{
	START,
	RUNNING,
	WAITING,
	DEAD;
}

