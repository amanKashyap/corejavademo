package coreJavaAssignments;

import java.io.IOException;

public class OpenNotepadUsingJava 
{
	public static void main(String[] args)
    {
        try
        {
            Runtime.getRuntime().exec("notepad.exe");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
