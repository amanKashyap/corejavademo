package coreJavaAssignments.employeeCreationSystem;

import java.util.HashMap;
import java.util.Iterator;

public class EmployeeAssignment 
{
	public static void main(String[] args) 
	{
		HashMap<Integer, Employee> hashMap = new HashMap<>();
		Employee e1 = new Employee(1,"Ashutosh","Senior System Engineer");
		Employee e2 = new Employee(2,"Pawan","Systems Engineer");
		Employee e3 = new Employee(3,"Jash","Senior Java Developer");
		Employee e4 = new Employee(4,"Neha","Senior System Engineer");
		Employee e5 = new Employee(5,"Tripti","Manager");
		
		hashMap.put(e1.getEmployeeId(), e1);
		hashMap.put(e2.getEmployeeId(), e2);
		hashMap.put(e3.getEmployeeId(), e3);
		hashMap.put(e4.getEmployeeId(), e4);
		hashMap.put(e5.getEmployeeId(), e5);
		
		//Searching an element by employee Id.
		Employee e = hashMap.get(4);
		if(e != null)
		{
			System.out.println("ID: "+e.getEmployeeId());
			System.out.println("Name: "+e.getEmployeeName());
			System.out.println("Designation: "+e.getEmployeeDesignation());
		}
		else
		{
			throw new EmployeeNotFoundException("Mentioned ID is Incorrect Please Enter a correct Employee ID.");
		}
		

	}

}
