package coreJavaAssignments;

public class Threading 
{
	public static void main(String[] args) 
	{
		myRun1 runnable1 = new myRun1();
		myRun2 runnable2 = new myRun2();
		
		Thread thread1 = new Thread(runnable1);
		Thread thread2 = new Thread(runnable2);
		
		try 
		{
			thread1.start();
			thread2.start();
			thread1.join();
			thread2.join();
		}
		catch (InterruptedException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
}

class myRun1 implements Runnable
{
	@Override
	public void run() 
	{
		for(int i= 1; i < 11 ; i++)
		{
			System.out.println(i);
		}		
	}	
}

class myRun2 implements Runnable
{
	@Override
	public void run() 
	{
		for(int i= 11; i<21 ; i++)
		{
			System.out.println(i);
		}		
	}	
}