package coreJavaAssignments;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.omg.CosNaming.IstringHelper;
import org.w3c.dom.Node;

public class CollectionAssignment 
{
	public static void main(String[] args) 
	{
		//ARRAY LIST
		System.out.println("****************************************Array List**********************************");
		ArrayList<Integer> arrayList = new ArrayList<>();
		System.out.println("Add 1 "+arrayList.add(1));
		System.out.println("Add 2 "+arrayList.add(2));
		System.out.println("Add 3 "+arrayList.add(3));
		System.out.println("Add 4 "+arrayList.add(4));
		System.out.println("Add 5 "+arrayList.add(5));
		System.out.println("Add 1 "+arrayList.add(1));
		
		/*ORDER: Insertion Order
				 Duplicates Allowed */
		System.out.println("ArrayList:-"+arrayList);
		
		//Conversion of Array List into Object []
		Object[] a = new Object[10];
		a = arrayList.toArray();
		for(Object o : a)
		{
			System.out.println(o);
		}
		
		System.out.println("ForEach Method");
		arrayList.forEach(i -> {
								 i= i*2;
								 System.out.print(i+" ");								
								});
		
		//LINKED LIST
		System.out.println("****************************************Linked List**********************************");
		LinkedList<Integer> linkedList = new LinkedList<>();
		linkedList.addAll(arrayList);
		System.out.println();
		/*ORDER: Insertion Order
		  Duplicates Allowed */
		System.out.println("LINKED LIST: "+linkedList);
		/*Added as Last Element - Internally linkLast method is called.
		  Which will basically add the element as the last element and change the Node.next to point to last */
		linkedList.add(2);
		//System.out.println("LINKED LIST: "+linkedList);
		
		/*
		 * Check whether the index is valid or not.
		 * call linkLast if index == size of linkedList.
		 * call linkBefore passing element and current node present at the mentioned index.
		 */
		linkedList.add(2,10);
		
		/*
		 * Calls removeFirst() which internally calls unlinkFirst() which would in return unlink the head from the linkedList.
		 */
		linkedList.remove();
		System.out.println("LINKED LIST "+linkedList);
		
		/*
		 * Check is Index is valid or not.
		 * Index start from 0.
		 * Unlink the node if the index is valid.
		 */
		linkedList.remove(2);
		System.out.println("LINKED LIST "+linkedList);
		
		//HASHSET
		System.out.println("****************************************Hash Set**********************************");
		HashSet<Integer> hashSet = new HashSet<>();
		/* 
		 * NO Insertion Order
		 * Hashing Algorithm
		 * NO Duplicates Allowed
		 */
		hashSet.addAll(linkedList);
		
		/*
		 * Add method Internally calls map.put.
		 * output of which is compared with null.
		 * element is entered in place of Key - Ensuring Uniqueness
		 * Value is Common PRESENT which is of object type.
		 * 
		 * Iterator returns an iterator of keySet of the internal map used.
		 */
		System.out.println("Adding Duplicate 5 "+ hashSet.add(5));
		System.out.println("HASHSET "+hashSet);
		
		//LINKEDHASH SET
		System.out.println("****************************************Linked Hash Set**********************************");
		
		LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<>();
		linkedHashSet.addAll(linkedList);
		linkedHashSet.add(3);
		/*
		 * Insertion Order
		 * No Duplicates
		 */
		System.out.println("LINKED HASH SET "+linkedHashSet);
		
		System.out.println("****************************************Tree Set**********************************");
		
		/*
		 * Sorting Order
		 * No Insertion Order
		 * No Duplicates
		 * 
		 * Comparator passed - sorting values decending.
		 */
		TreeSet<Integer> treeSet = new TreeSet<>((i1,i2)-> i2.compareTo(i1));
		treeSet.addAll(linkedList);
		System.out.println(treeSet);
		System.out.println("CEILING: "+treeSet.ceiling(4));
		System.out.println("TAILSET :"+treeSet.tailSet(4));
		System.out.println("HEADSET :"+treeSet.headSet(4));
		
		System.out.println("****************************************Linked Hash Map**********************************");
		
		LinkedHashMap<Integer, String> linkedHashMap = new LinkedHashMap<>();
		for(int i=1; i<5 ; i++)
		{
			System.out.print("ADDING ID #"+i);
			/*
			 * Insertion Order
			 * Hashing and Singly Linked List
			 * No Duplicates in Keys
			 * Duplicates in Values
			 */
			System.out.println(linkedHashMap.put(i, "Entry"+i));
			System.out.print("ADDING ID #"+(i-1));
			System.out.println(linkedHashMap.put(i-1,"Entry"+(i-1)));
		}
		
		System.out.println("SIZE:"+linkedHashMap.size());
		
		Collection<String> values = linkedHashMap.values();
		for(String value : values)
			{
				System.out.println("Value :"+value);
			}
		
		System.out.println("****************************************Tree Map**********************************");
		/*
		 * No Insertion Order
		 * Sorting Order - Default Natural Sorting Order - Ascending
		 * No Duplicates
		 */
		TreeMap<Integer, String> treeMap = new TreeMap<>(linkedHashMap);
		Collection<String> treeValues = treeMap.values();
		for(String value : treeValues)
		{
				System.out.println("Value :"+value);
		}
		
	}

}
