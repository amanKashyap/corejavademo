package coreJavaAssignments;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class SortTrainNumberTimeExampleUsingCollection 
{
	public static void main(String[] args) 
	{
		List<String> trainData = new ArrayList<>();
		List<Train> trainObject = new ArrayList<>(); 
		
		trainData.add("4210|10:21|Pune");
		trainData.add("4211|10:22|Lonavala");
		trainData.add("4212|10:23|Lonavala");
		trainData.add("4211|10:01|Pune");
		trainData.add("4212|10:29|Karjat");
		trainData.add("4214|12:30|Karjat");
		trainData.add("4210|10:45|Lonavala");
		trainData.add("4211|11:02|Karjat");
		trainData.add("4214|11:05|Lonavala");
		trainData.add("4210|12:45|Mumbai");
		trainData.add("4210|11:50|Karjat");
		trainData.add("4214|12:55|Mumbai");
		trainData.add("4211|12:01|Mumbai");
		trainData.add("4212|11:51|Mumbai");
		trainData.add("4212|09:51|Pune");

		for(String s : trainData)
		{
			String[] str = s.split("\\|",3);
			Train train = new Train();
			train.setTrainNumber(Integer.parseInt(str[0]));
			train.setTime(str[1]);
			train.setLocation(str[2]);			
			
			trainObject.add(train);
		}
		
		Set<Train> trainSet = new TreeSet<>(new myTrainComp());
		trainSet.addAll(trainObject);
		for(Train t : trainSet)
		{
			System.out.println(t.getTrainNumber() +"|"+ t.getTime() +"|"+ t.getLocation());
		}
		
	}

}

class Train
{
	private Integer trainNumber;
	private String time;
	private String location;
	
	public Integer getTrainNumber() {
		return trainNumber;
	}
	public void setTrainNumber(Integer trainNumber) {
		this.trainNumber = trainNumber;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}	
}

class myTrainComp implements Comparator<Train>
{
	@Override
	public int compare(Train o1, Train o2) 
	{
		
		return Comparator.comparing(Train :: getTrainNumber)
						 .thenComparing(Train :: getTime)
						 .compare(o1, o2);
	}
}