package coreJavaAssignments;

public class SumOfPairs {

	public static void main(String[] args) 
	{
		
		SumOfPairs.findThePairs(new int[] {4, 6,5,-10,8,5,20}, 10);
	}
	
	static void findThePairs(int[] inputArray, int inputNumber)
	{
		int[] inputIntArray = inputArray;
		int lengthOfArray = inputIntArray.length;
		
		for(int i =0; i < lengthOfArray ; i++)
		{
			for(int j= i+1; j< lengthOfArray; j++)
			{						
				
				if((inputIntArray[i]+inputIntArray[j])== inputNumber)
				{
					System.out.println("Pair of Elements whose sum is "+inputNumber+" are "+ inputIntArray[i]+" and "+inputIntArray[j]);
				}
			}
		}
	}

}
