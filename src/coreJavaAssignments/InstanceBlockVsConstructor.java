package coreJavaAssignments;

public class InstanceBlockVsConstructor 
{
	int m;
	 
    double n;
 
    public InstanceBlockVsConstructor(int i)
    {
        m = i;
    }
 
    public InstanceBlockVsConstructor(double d)
    {
        n = d;
    }
 
    {
        System.out.println(m);
 
        System.out.println(n);
    }
 
    public static void main(String[] args)
    {
    	InstanceBlockVsConstructor a1 = new InstanceBlockVsConstructor(500);
 
    	InstanceBlockVsConstructor a2 = new InstanceBlockVsConstructor(50.0);
 
        System.out.println(a1.m+" : "+a1.n);
 
        System.out.println(a2.m+" : "+a2.n);
    }
}

//OUTPUT
/*
0
0.0
0
0.0
500 : 0.0
0 : 50.0
*/

/*
 Instance block would be called before the constructor.
 */
