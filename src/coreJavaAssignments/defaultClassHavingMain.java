package coreJavaAssignments;

class defaultClassHavingMain 
{
	public static void main(String[] args)
	{
		try
		{
			System.out.println("Statement 1");
			System.out.println(10/0);
			System.out.println("Statement 2");
		}
		catch(Exception e)
		{
			System.out.println("Statement 3");
		}
		System.out.println("Statement 4");
	}
}

/*
 *OUTPUT: 

Statement 1
Statement 3
Statement 4
*/