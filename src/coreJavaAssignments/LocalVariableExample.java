package coreJavaAssignments;

public class LocalVariableExample {

	static
    {
        d = 10;
 
        //System.out.println(d);
        /*
         * variables can be initialized before they are declared. 
         * But you can�t use the variables before they are declared.
         */
    }
 
    static double d;

	public static void main(String[] args) 
	{
		String s;
		 
        //System.out.println(s);
		/* 
		 * Compile Time Error, Local Variable must be initialized before using.
		 */

	}

}
