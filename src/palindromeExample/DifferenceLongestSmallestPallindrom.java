package palindromeExample;

public class DifferenceLongestSmallestPallindrom {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		DifferenceLongestSmallestPallindrom test = new DifferenceLongestSmallestPallindrom();
		System.out.println(test.difference("abac"));
		System.out.println(test.difference("level"));

	}
	public int difference(String input)
	{
		StringBuilder str = new StringBuilder(input);
		int len = str.length();
		int subStrLen1 = 0;
		int subStrLen2 = 0;
		boolean flag = true;
		for( int i =0; i <len ; i++)
		{
			System.out.println(str);
			if(str.charAt(i) != str.charAt(len-1))
			{				
				flag = false;
				break;
			}			
		}
		if(flag)
		{			
			return len-1;			
		}
		else
		{
			System.out.println("Substr1:"+input.substring(1, len));
			subStrLen1 = difference(input.substring(1, len));
			System.out.println("Substr2:"+input.substring(0, len-1));
			subStrLen2 = difference(input.substring(0,len));
		}
		if(subStrLen1>=subStrLen1)
		{
			return subStrLen1-1;
		}
		else
		{
			return subStrLen2-1;
		}		
	}
}
