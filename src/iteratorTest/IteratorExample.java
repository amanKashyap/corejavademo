package iteratorTest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class IteratorExample 
{

	public static void main(String[] args) 
	{
		List<Integer> a = new ArrayList<>();
		a.add(1);
		a.add(2);
		a.add(3);
		List<Integer> b = new ArrayList<>();
		b.add(5);
		b.add(6);
		b.add(7);
		//b.add(8);
		
		
		for(Iterator<Integer> a1 = a.iterator(); a1.hasNext();)
		{
			for(Iterator<Integer> b1 = b.iterator(); b1.hasNext();)
			{
				Integer aNext = a1.next();
				System.out.println("A.next-->"+aNext);
				Integer bNext = b1.next();
				System.out.println("B.next-->"+bNext);
				System.out.println(aNext+bNext);
			}
		}
	}

}
